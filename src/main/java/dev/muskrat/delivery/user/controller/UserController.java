package dev.muskrat.delivery.user.controller;

import dev.muskrat.delivery.user.dto.*;
import dev.muskrat.delivery.user.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping("/{id}")
    public UserDTO findById(
        @PathVariable Long id
    ) {
        return userService.findById(id);
    }

    @PatchMapping("/update")
    public UserUpdateResponseDTO update(
        @Valid @RequestBody UserUpdateDTO userUpdateDTO
    ) {
        return userService.update(userUpdateDTO);
    }

    @GetMapping("/email/{email:.+}")
    public UserDTO findByEmail(
        @NotNull @PathVariable String email
    ) {
        return userService.findByEmail(email);
    }

    @PostMapping("/page")
    public UserPageDTO page(
        @Valid @RequestBody(required = false) UserPageRequestDTO userPageRequestDTO,
        @PageableDefault(size = 20, sort = {"id"}, direction = Sort.Direction.DESC) Pageable page
    ) {
        return userService.page(userPageRequestDTO, page);
    }
}
