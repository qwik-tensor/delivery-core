package dev.muskrat.delivery.utils;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
public interface AbstractTest {
}
